# Plants CRUD

Simple CRUD backend for collecting plants. Plants get saved to SQLite... in memory.

| Endpoint                 | Description              | JSON body params      |
| ------------------------ | ------------------------ | --------------------- |
| `GET /api/plants`        | Lists plants             |                       |
| `GET /api/plants/:id`    | Retrieves a plant by ID  |                       |
| `POST /api/plants`       | Creates a plant          | `name, height, color` |
| `PATCH /api/plants/:id`  | Updates a plant          | `name, height, color` |
| `DELETE /api/plants/:id` | Deletes a plant          |                       |
| `GET /random`            | Retrieves a random plant |                       |
| `GET /stats`             | Returns plant statistics |                       |

## How it works

The entrypoint is in `index.ts`, which imports a setup function from `server.ts` and launches the server. The setup function gets a `Dispatch` instance from the `inversify` container. `Dispatch` is injected with any `Controller` instances, calls their `getRoutes` methods and returns the combined routes. Sequelize models are injected into controllers with `inversify`.

Jest bypasses the entrypoint and imports the setup function directly, preventing port clashes when running in parallel.

## How to run

- `yarn dev` to launch auto-restarting dev server.
- `yarn build` to create a production build.
- `yarn start` to start a production build.
- `yarn lint(:fix)` to run eslint.
- `yarn test` to run tests in Jest.

## Environment variables

Variables are loaded from `.env`.

| Variable   | Description          | Default value           |
| ---------- | -------------------- | ----------------------- |
| `PORT`     | Port to listen on    | `5000`                  |
| `BASE_URI` | Base application URI | `http://localhost:5000` |
