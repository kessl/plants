import { setup } from './server'
import logger from 'loglevel'

logger.setLevel('info')

const app = setup()

const server = app.listen(process.env.PORT, () => {
  logger.info(`Listening on`, server.address())
})
