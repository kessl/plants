import { Sequelize } from 'sequelize-typescript'
import { Plant } from './plants/PlantModel'

export const sequelize = new Sequelize('sqlite::memory:')

sequelize.addModels([Plant])

sequelize.sync()
