import { Container } from 'inversify'
import { Controller } from './interfaces'
import { PlantController } from './plants/PlantController'
import { Plant } from './plants/PlantModel'
import { TYPES } from './types'

const container = new Container()

// controllers
container.bind<Controller>(TYPES.Controller).to(PlantController)

// models
container.bind<typeof Plant>(TYPES.PlantModel).toConstantValue(Plant)

export { container }
