import { Router } from 'express'

export type Controller = {
  getPath: () => string
  getRoutes: () => Router
}
