import request from 'supertest'
import { setup } from '../server'

function validatePlant(data: any, plant: any) {
  if (!data) throw new Error('Undefined plant')
  if (!data.id) throw new Error('Missing plant.id key')
  if (!data.updatedAt) throw new Error('Missing plant.updatedAt key')
  if (!data.createdAt) throw new Error('Missing plant.createdAt key')
  if (data.name !== plant.name) throw new Error('plant.name did not match')
  if (data.height !== plant.height) throw new Error('plant.height did not match')
}

const app = setup()
let plant = { name: 'test-plant', height: 100, color: 'green' }
let id: string | undefined

describe('Plants CRUD', () => {
  it('Should return empty response', done => {
    request(app).get('/api/plants').expect(200, { data: [] }, done)
  })

  it('Should insert a plant', done => {
    request(app)
      .post('/api/plants')
      .send(plant)
      .expect(201)
      .expect((res: any) => {
        validatePlant(res.body.data, plant)
        id = res.body.data.id
      })
      .end(done)
  })

  it('Should return inserted plant in list', done => {
    request(app)
      .get('/api/plants')
      .expect(200)
      .expect((res: any) => validatePlant(res.body.data[0], plant))
      .end(done)
  })

  it('Should return inserted plant by ID', done => {
    request(app)
      .get(`/api/plants/${id}`)
      .expect(200)
      .expect((res: any) => validatePlant(res.body.data, plant))
      .end(done)
  })

  it('Should update plant', done => {
    plant = { name: 'test-edited-plant', height: 200, color: 'purple' }
    request(app)
      .patch(`/api/plants/${id}`)
      .send(plant)
      .expect(200)
      .expect((res: any) => validatePlant(res.body.data, plant))
      .end(done)
  })

  it('Should delete plant', done => {
    request(app).delete(`/api/plants/${id}`).expect(204).end(done)
  })

  it('Should return 404 if plant not found', done => {
    request(app).delete(`/api/plants/${id}`).expect(404).end(done)
  })
})
