import bodyParser from 'body-parser'
import { config } from 'dotenv'
import express, { ErrorRequestHandler } from 'express'
import 'express-async-errors'
import logger from 'loglevel'
import { Dispatch } from './dispatch'
import { container } from './inversify.config'
import './sequelize'

config()

export function setup() {
  const app = express()

  app.use(bodyParser.json())

  const dispatch = container.resolve<Dispatch>(Dispatch)
  const routes = dispatch.getRoutes()
  app.use('/api', routes)

  app.use(errorMiddleware)

  return app
}

const errorMiddleware: ErrorRequestHandler = (error, req, res, next) => {
  if (res.headersSent) {
    return next(error)
  }

  logger.error(error)
  res.status(500)
  res.json({
    message: error.message,
    ...(process.env.NODE_ENV === 'production' ? null : { stack: error.stack }),
  })
}
