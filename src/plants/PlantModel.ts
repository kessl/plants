import { injectable } from 'inversify'
import { Column, Model, Table } from 'sequelize-typescript'

@injectable()
@Table
export class Plant extends Model {
  @Column
  get name(): string {
    return this.getDataValue('name')
  }

  set name(value: string) {
    this.setDataValue('name', value)
  }

  @Column
  get height(): number {
    return this.getDataValue('height')
  }

  set height(value: number) {
    this.setDataValue('height', value)
  }

  @Column
  get color(): string {
    return this.getDataValue('color')
  }

  set color(value: string) {
    this.setDataValue('color', value)
  }
}
