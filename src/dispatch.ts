import express from 'express'
import { injectable, multiInject } from 'inversify'
import 'reflect-metadata'
import { Controller } from './interfaces'
import { TYPES } from './types'

@injectable()
export class Dispatch {
  @multiInject(TYPES.Controller) private _controllers!: Controller[]

  public getRoutes() {
    const router = express.Router()
    this._controllers.forEach(controller =>
      router.use('/' + controller.getPath(), controller.getRoutes())
    )
    return router
  }
}
