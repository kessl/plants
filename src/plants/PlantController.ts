import express, { RequestHandler } from 'express'
import { inject, injectable } from 'inversify'
import { Controller } from '../interfaces'
import { TYPES } from '../types'
import { Plant } from './PlantModel'
import _ from 'lodash'

@injectable()
export class PlantController implements Controller {
  @inject(TYPES.PlantModel) private _model!: typeof Plant

  public getPath() {
    return 'plants'
  }

  public getRoutes() {
    const router = express.Router()

    router.get('/', this.getPlants)
    router.get('/stats', this.getStats)
    router.get('/random', this.getRandom)
    router.get('/:id', this.getPlant)

    router.post('/', this.insertPlant)
    router.patch('/:id', this.updatePlant)
    router.delete('/:id', this.deletePlant)

    return router
  }

  getPlants: RequestHandler = async (req, res) => {
    const plants = await this._model.findAll()

    res.json({ data: plants })
  }

  getPlant: RequestHandler = async (req, res) => {
    if (!req.params.id) {
      return res.status(400).json({ errors: [{ code: 'MISSING_ID' }] })
    }

    const plant = await this._model.findByPk(req.params.id)

    if (!plant) {
      return res.status(404).json({ errors: [{ code: 'NOT_FOUND', id: req.params.id }] })
    }

    res.json({ data: plant })
  }

  getStats: RequestHandler = async (req, res) => {
    const plants = await this._model.findAll()

    res.json({
      data: {
        total: plants.length,
        totalHeight: plants.reduce((acc, current) => acc + current.height, 0),
        totalByColor: _.countBy(plants, 'color'),
        names: plants.map(plant => plant.name),
      },
    })
  }

  getRandom: RequestHandler = async (req, res) => {
    const plants = await this._model.findAll()

    res.json({
      data: _.sample(plants),
    })
  }

  insertPlant: RequestHandler = async (req, res) => {
    if (!req.body.name) {
      return res.status(400).json({ errors: [{ code: 'MISSING_NAME' }] })
    }

    if (!req.body.height) {
      return res.status(400).json({ errors: [{ code: 'MISSING_HEIGHT' }] })
    }

    if (!req.body.color) {
      return res.status(400).json({ errors: [{ code: 'MISSING_COLOR' }] })
    }

    const plant = await this._model.create({
      name: req.body.name,
      height: req.body.height,
      color: req.body.color,
    })

    res
      .status(201)
      .header('Location', `${process.env.BASE_URI}/api/plants/${plant.id}`)
      .json({ data: plant.toJSON() })
  }

  updatePlant: RequestHandler = async (req, res) => {
    if (!req.params.id) {
      return res.status(400).json({ errors: [{ code: 'MISSING_ID' }] })
    }

    if (!req.body.name && !req.body.height && !req.body.color) {
      return res.status(204).end()
    }

    const plant = await this._model.findByPk(req.params.id)

    if (!plant) {
      return res.status(404).json({ errors: [{ code: 'NOT_FOUND', id: req.params.id }] })
    }

    if (req.body.name) {
      plant.name = req.body.name
    }

    if (req.body.height) {
      plant.height = req.body.height
    }

    if (req.body.color) {
      plant.color = req.body.color
    }

    await plant.save()
    res.status(200).json({ data: plant.toJSON() })
  }

  deletePlant: RequestHandler = async (req, res) => {
    if (!req.params.id) {
      return res.status(400).json({ errors: [{ code: 'MISSING_ID' }] })
    }

    const plant = await this._model.findByPk(req.params.id)

    if (!plant) {
      return res.status(404).json({ errors: [{ code: 'NOT_FOUND', id: req.params.id }] })
    }

    await plant.destroy()
    res.status(204).end()
  }
}
